<?php


use Authentication\PasswordHasher\DefaultPasswordHasher;
use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
          [
              'email' => 'user1',
              'password' => (new DefaultPasswordHasher())->hash('123456'),
              'created' => date("Y-m-d H:i:s"),
              'modified' => date("Y-m-d H:i:s")
          ],
            [
                'email' => 'user2',
                'password' => (new DefaultPasswordHasher())->hash('123456'),
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s")
            ]
        ];
        $user = $this->table('users');
        $user->insert($data)->saveData();
    }
}
