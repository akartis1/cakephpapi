<?php
/**
 * @author <Akartis>
 * (c) akartis-dev <sitrakaleon23@gmail.com>
 * Do it with love
 */

namespace App\Model\Entity;


use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity
{
    protected $_hidden = ['password'];

    protected function _setPassword(string $password): ?string
    {
        if ($password !== '') {
            return (new DefaultPasswordHasher())->hash($password);
        }
    }
}
