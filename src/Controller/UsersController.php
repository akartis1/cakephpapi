<?php
/**
 * @author <Akartis>
 * (c) akartis-dev <sitrakaleon23@gmail.com>
 * Do it with love
 */

namespace App\Controller;

use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Class UsersController
 * @package App\Controller
 * @property AuthenticationComponent $Authentication
 */
class UsersController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['login']);
    }

    public function login()
    {
        $this->request->allowMethod('post');
        $result = $this->Authentication->getResult();

        if($result->isValid()){
            $payload = [
                "sub" => $result->getData()->id,
                "ext" => time() + 3600
            ];
            $jwt = JWT::encode($payload, Security::getSalt());
            $response = ['status' => 200, 'data' => "", "token" => $jwt];
        }else{
            $response = ['status' => 401, 'data' => "", 'message' => "Invalid credential"];
        }
        $this->set(['response' => $response, '_serialize' => ['response']]);
    }
}
