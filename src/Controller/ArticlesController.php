<?php
/**
 * @author <Akartis>
 * (c) akartis-dev <sitrakaleon23@gmail.com>
 * Do it with love
 */

namespace App\Controller;

use Authentication\Controller\Component\AuthenticationComponent;

/**
 * Class ArticlesController
 * @package App\Controller
 * @property AuthenticationComponent $Authentication
 */
class ArticlesController extends AppController
{
    private const PAGINATOR = [
        'limit' => 5,
        'order' => ['Articles.id' => 'asc']
    ];

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $articles = $this->Articles
            ->find()
            ->contain('Users');
        $response = ['status' => 200, 'data' => $this->Paginator->paginate($articles, self::PAGINATOR)];
        $this->set(['response' => $response, '_serialize' => ['response']]);
    }

    public function view(string $id)
    {
        $article = $this->Articles->findById($id)->contain('Users');
        $response = ['status' => 200, 'data' => $article];
        $this->set(['response' => $response, '_serialize' => ['response']]);
    }

    public function add()
    {
        $article = $this->Articles->newEntity($this->request->getData());
        $article->user_id = $this->Authentication->getIdentity()->id;
        if ($this->Articles->save($article)) {
            $response = ['status' => 201, 'data' => [], 'message' => 'Articles ajouter avec succes'];
        } else {
            $response = ['status' => 400, 'data' => [], 'message' => "Une erreur s'est produite"];
        }

        $this->set(['response' => $response, '_serialize' => ['response']]);
    }

    public function edit(string $id)
    {
        $article = $this->Articles->findById($id)->contain('Users')->firstOrFail();
        $response = ['status' => 404, 'data' => [], 'message' => 'Articles non trouver'];

        if ($article) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());

            if ($this->Articles->save($article)) {
                $response = ['status' => 200, 'data' => [], 'message' => 'Articles bien modifier avec succes'];
            }
        }
        $this->set(['response' => $response, '_serialize' => ['response']]);
    }

    public function delete(string $id)
    {
        $article = $this->Articles->findById($id)->contain('Users')->firstOrFail();
        $response = ['status' => 404, 'data' => [], 'message' => 'Articles non trouver'];

        if ($article) {
            $article = $this->Articles->delete($article, $this->request->getData());
            $response = ['status' => 200, 'data' => [], 'message' => 'Articles supprimer avec succes'];
        }
        $this->set(['response' => $response, '_serialize' => ['response']]);
    }
}
