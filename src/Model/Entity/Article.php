<?php
/**
 * @author <Akartis>
 * (c) akartis-dev <sitrakaleon23@gmail.com>
 * Do it with love
 */

namespace App\Model\Entity;


use Cake\ORM\Entity;
use Cocur\Slugify\Slugify;

class Article extends Entity
{
    protected $_accessible = [
        'title' => true,
        'content' => true,
        '*' => false
    ];

    protected function _setTitle(string $title)
    {
        $this->slug = (new Slugify())->slugify($title);

        return $title;
    }
}
