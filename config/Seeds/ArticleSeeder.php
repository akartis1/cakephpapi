<?php


use Cocur\Slugify\Slugify;
use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class ArticleSeeder extends AbstractSeed
{
    public function getDependencies()
    {
        return ['UserSeeder'];
    }

    public function run()
    {
        $faker = Factory::create();
        $data = [];
        for ($i = 0; $i < 30; $i++) {
            $title = $faker->sentence;
            $data[] = [
                'title' => $title,
                'slug' => (new Slugify())->slugify($title),
                'content' => $faker->sentences(10, true),
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s"),
                'user_id' => $faker->randomElement($this->getAllUser())
            ];
        }
        $articles = $this->table('articles');
        $articles->insert($data)->saveData();
    }

    public function getAllUser(): array
    {
        $ids = $this->query('SELECT id FROM users');
        $result = [];
        foreach ($ids as $id) {
            $result[] = $id['id'];
        }
        return $result;
    }
}
